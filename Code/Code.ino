
const int redPin = 9;
const int greenPin = 10;
const int bluePin = 11;

void setup() {

  Serial.begin(9600);
  

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  

  analogWrite(redPin, 0);
  analogWrite(greenPin, 0);
  analogWrite(bluePin, 0);
}

void loop() {

  if (Serial.available() > 0) {

    char input = Serial.read();
    
 
    switch (input) {
      case 'r':
        analogWrite(redPin, 255); 
        analogWrite(greenPin, 0);
        analogWrite(bluePin, 0);
        break;
        
      case 'g':
        analogWrite(redPin, 0); 
        analogWrite(greenPin, 255);
        analogWrite(bluePin, 0); 
        break;
        
      case 'b':
        analogWrite(redPin, 0); 
        analogWrite(greenPin, 0); 
        analogWrite(bluePin, 255); 
        break;
        
      case 'o':
        analogWrite(redPin, 255);  
        analogWrite(greenPin, 255);
        analogWrite(bluePin, 255);
        break;
        
      case 'f':
        analogWrite(redPin, 0);   
        analogWrite(greenPin, 0); 
        analogWrite(bluePin, 0);  
        break;
        
      default:
        
        break;
    }
  }
}
